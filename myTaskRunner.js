var chalk= require('chalk'),
    ana = require('./analise');

var argCmd =process.argv.slice(2),
    arg={};

const repoCred = {
    branch : "pr_check_branch1",
    keyStr: "Basic "+Buffer.from('d4955159576d8754acbdc48edc794ebc89432088').toString('base64'),
    baseRepoApiUrlDef: "https://github5.cisco.com/api/v3/repos/ccbu-shared/common-desktop",
    commentSuffix :"\n\n*Automatically added by static-code-analyser*"
};

var createArg=()=>{
    /*console.log(chalk.cyan('\n' +
        '   _____ _                  _____           _                              \n' +
        '  / ____(_)                / ____|         | |                             \n' +
        ' | |     _ ___  ___ ___   | (___  _   _ ___| |_ ___ _ __ ___  ___          \n' +
        ' | |    | / __|/ __/ _ \\   \\___ \\| | | / __| __/ _ \\ \'_ ` _ \\/ __|   \n' +
        ' | |____| \\__ \\ (_| (_) |  ____) | |_| \\__ \\ ||  __/ | | | | \\__ \\   \n' +
        '  \\_____|_|___/\\___\\___/  |_____/ \\__, |___/\\__\\___|_| |_| |_|___/   \n' +
        '                                   __/ |                                   \n' +
        '                                  |___/                                    \n'));*/
    argCmd.forEach(o=>{
        let prm=o.split("=");
        arg[prm[0].substring(2)]=prm[1]
    });
};

var runScrpt=()=>{
    arg = repoCred;
    ana.triverceGit(arg, (err,success)=> {
        console.log("err,success: ",JSON.stringify({err,success},null,4));
    });
};
createArg();
runScrpt();
